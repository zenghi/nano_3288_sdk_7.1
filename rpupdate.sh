#!/bin/bash

LINUX_UPDATE="linux_update/rockdev/Image/"
if [ ! -d $LINUX_UPDATE ]
        then
        {
                cd linux_update/rockdev/
                mkdir Image
                chmod -R 777 Image
                cd -
        }
fi
cp u-boot/rk3288_loader_v1.06.233.bin linux_update/rockdev/MiniLoaderAll.bin
cp u-boot/trust.img 				$LINUX_UPDATE
cp u-boot/uboot.img 				$LINUX_UPDATE
cp kernel/resource.img 				$LINUX_UPDATE
cp kernel/kernel.img 				$LINUX_UPDATE
cp rockdev/Image-rk3288/misc.img 		$LINUX_UPDATE
cp rockdev/Image-rk3288/boot.img 		$LINUX_UPDATE
cp rockdev/Image-rk3288/recovery.img 		$LINUX_UPDATE
cp rockdev/Image-rk3288/system.img 		$LINUX_UPDATE
cp rockdev/Image-rk3288/parameter.txt		$LINUX_UPDATE
cp rockdev/Image-rk3288/vendor0.img 		$LINUX_UPDATE
cp rockdev/Image-rk3288/vendor1.img 		$LINUX_UPDATE

pushd linux_update/rockdev
. mkupdate.sh
if [ $? -eq 0 ]; then
        echo "update.img make OK!"
else
        echo "update.img make FAILED!"
        exit 1
fi
popd
chmod 777 linux_update/rockdev/update.img
DATE_CURRENT=$(date +%Y%m%d_%k%M)
mv linux_update/rockdev/update.img rockdev/Image-rk3288/update_"$DATE_CURRENT".img
rm -rf $LINUX_UPDATE/*
